#include "stack.h"
void init(stack *s){
	s->i = 0;
}
void push(int num, stack *s){
	s->a[s->i] = num;
	s->i = s->i + 1;
}
int pop(stack *s){
	int x;
	(s->i)--;
	x = s->a[s->i];
	return x;
}
int isempty(stack *s){
	if(s->i <= 0){
		return 1;
	}
	else
		return 0;
}
int isfull(stack *s){
	if(s->i == MAX + 1){
		return 1;
	}
	else
		return 0;
}		
