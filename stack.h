#define MAX 32
typedef struct stack{
	int i;
	int a[MAX];
}stack;
void init(stack *s);
void push(int num, stack *s);
int pop(stack *s);
int isempty(stack *s);
int isfull(stack *s);
