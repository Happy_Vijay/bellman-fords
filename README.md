Bellman Fords Algorithm:
Its used to find shortest path between two edges when negative cycles are present in the graph.
The program takes a CSV file.
The CSV file has distance between the different cities in form

CSV File

<city name 1> <city name 2> <distance between city1 and city2>
<city name > <city name> <distance>
.           .           .
.           .           .
.           .           .


The programs builds a graph using this CSV File.
Adjacency matrix representation, in which each city name is associated with a vertex.

Now the program runs the Bellman Fords algorithm on it.

The algorithm flow goes as follows
1) First of all, build an array of edges.
the definition of an edge is struct edge {  int v1, v2, w};

2) Have a parent array and distance array.
make distance[source] = 0;
and distance [of all other vertices] = INF;

make parent[of all vertices] = -1

3)Run a loop of E times

In each iteration check if 
v1 = edge[i].v1;
v2 = edge[i].v2;
w = edge[i].w;

if(distance[v1] + w < distance[v2])
    distance[v2] = distance[v1] + w;
 
4) Repeat the process for V - 1, times if the distance array still changes then there's a negative cycle.