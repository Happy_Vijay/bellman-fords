#include"graph.h"
#include<stdio.h>
#include<stdlib.h>
int main(int argc, char *argv[]){
	graph g;
	result r;
	if(argc != 3){
		printf("Arguments to main:\n");
		printf("<distance file> <name of city>\n");
		exit(1);
	}
	buildGraphFromFile(&g, argv[1]);
	r = bellmanFord(&g, argv[2]);
	printPath(&g, r, argv[2]);
	return 0;
}
