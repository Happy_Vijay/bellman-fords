typedef struct graph{
	int n;
	int **a;
	char **cityNames;
}graph;
typedef struct result{
	int *distance;
	int *parent;
}result;
typedef struct edge{
	int v1;
	int v2;
	int w;
}edge;
/* Function Prototype declarations */
void initGraph(graph *g, int n);
void buildGraphFromFile(graph *g, char *filename);
result bellmanFord(graph *g, char *source);
void printPath(graph *g, result r, char *source);
void printGraph(graph *g);
int getVertex(graph *g, char *source);
int getInfinity(graph *g);
