#include"graph.h"
#include<stdlib.h>
#include<string.h>
#include<stdio.h>
#include<limits.h>
#include"stack.h"
void initGraph(graph *g, int n){
	int i, j;
	g -> n = n;
	g->a = (int **)malloc(sizeof(int *) * n);
	for(i = 0; i < n; i++){
		g->a[i] = (int *)malloc(sizeof(int) * n);
		for(j = 0; j < n; j++){
			g->a[i][j] = 0;
		}
	}
}
int getInfinity(graph *g){
	int i, j, n = g->n;
	int infinity = 0;
	for(i = 0; i < n; i++){
		for(j = 0; j < n; j++){
			if(g->a[i][j] < 0){
				infinity = infinity - g->a[i][j];
			}
			else{
				infinity = infinity + g->a[i][j];
			}
		}
	}
	return infinity;
}
result bellmanFord(graph *g, char *source){
	int i, j, k;
	int v;	//Vertex no. of source
	int N;	//No. of edges
	int n;	//No. of vertices
	edge *e;	//Edge array
	int *distance;	//Distance array
	int *parent;	//Parent array
	result r;
	int infinity;
	int sum;
	n = g -> n;
	/* Find vertex no. of source city */
	v = getVertex(g, source);
	/* Find the infinity !*/
	infinity = getInfinity(g);
	/* Initializtion of arrays */
	/* distance array */
	distance = (int *)malloc(sizeof(int) * n);
	if(!distance){
		exit(1);
	}
	for(i = 0; i < n; i++){
		distance[i] = infinity;
	}
	distance[v] = 0;
	/* parent array */
	parent = (int *)malloc(sizeof(int) * n);
	if(!parent){
		exit(1);
	}
	for(i = 0; i < n; i++){
		parent[i] = -1;
	}
	/* Find no. of edges(N) */
	N = 0;
	for(i = 0; i < n; i++){
		for(j = 0; j < n; j++){
			if(g->a[i][j])
				N++;
		}	
	}
	/* array of edges */
	e = (edge *)malloc(sizeof(edge) * N);
	if(!e){
		exit(1);
	}
	k = 0;
	for(i = 0; i < n; i++){
		for(j = 0; j < n; j++){
			if(g->a[i][j]){
				e[k].v1 = i;
				e[k].v2 = j;
				e[k].w = g->a[i][j];
				k++;
			}
		}	
	}
	/*for(i = 0; i < N; i++){
		printf("%s %s %d\n", g->cityNames[e[i].v1], g->cityNames[e[i].v2], e[i].w);
	}*/
	/* Initialization of array completed*/
	/* Step 2: Updating distance and parent array */
	for(i = 0; i < n - 1; i++){
		for(j = 0; j < N; j++){
			sum = distance[e[j].v1] + e[j].w;
			if(sum < distance[e[j].v2]){
				distance[e[j].v2] = sum;
				parent[e[j].v2] = e[j].v1;
			}
		}
	}
	/* Step 3: Check for negative cycle */
	for(j = 0; j < N; j++){
		sum = distance[e[j].v1] + e[j].w;
		if(sum < distance[e[j].v2]){
			fprintf(stderr, "Negative cycle Detected");
			exit(1);
		}
	}
	r.distance = distance;
	r.parent = parent;
	return r;
}
void buildGraphFromFile(graph *g, char *filename){
	int i, n = 0;	/* n is the no. of vertices */
	int size = 128;	/* size is the maximum no. of vertices the file can accodomate */
	char **cityNames;
	FILE *fp;
	char *t1, *t2, *t3, line[128];
	int v1, v2, weight;
	fp = fopen(filename, "r");
	if(fp == NULL){
		return;
	}
	cityNames = (char **)malloc(sizeof(char*) * size);
	while(fgets(line, 128, fp)){
		t1 = strtok(line, ",");
		t2 = strtok(NULL, ",");
		t3 = strtok(NULL, ",");
		for(i = 0; i < n; i++){
			if(!strcasecmp(cityNames[i], t1)){
				/* Vertice is already present, no need to add it */
				break;
			}
		}
		/* New vertice found */
		if(i == n){
			cityNames[i] = (char *)malloc(strlen(t1) + 1);
			strcpy(cityNames[i], t1);
			n++;
		}
		if(i == size){
			size = size << 1;
			cityNames = (char **)realloc(cityNames, size * (sizeof(char *)));
		}
		/* Repeat the same for t2 */
		for(i = 0; i < n; i++){
			if(!strcasecmp(cityNames[i], t2)){
				/* Vertice is already present, no need to add it */
				break;
			}
		}
		/* New vertice found */
		if(i == n){
			cityNames[i] = (char *)malloc(strlen(t2) + 1);
			strcpy(cityNames[i], t2);
			n++;
		}
		if(i == size){
			size = size << 1;
			cityNames = (char **)realloc(cityNames, size * (sizeof(char *)));
		}
	}
	/* All vertices added to the graph */
	initGraph(g, n);
	fseek(fp, 0, SEEK_SET);			//or rewind(fp);
	while(fgets(line, 128, fp)){
		t1 = strtok(line, ",");
		t2 = strtok(NULL, ",");
		t3 = strtok(NULL, ",");
		for(i = 0; i < n; i++){
			if(!strcasecmp(cityNames[i], t1)){
				v1 = i;
				break;
			}
		}
		for(i = 0; i < n; i++){
			if(!strcasecmp(cityNames[i], t2)){
				v2 = i;
				break;
			}
		}
		weight = atoi(t3);
		g->a[v1][v2] = weight;
	}
	g -> cityNames = cityNames;
	fclose(fp);
	/* That's it, Graph is built */
}
void printPath(graph *g, result r, char *source){
	int i, v;
	int infinity;
	int par = 0, p;
	stack s;
	init(&s);
	v = getVertex(g, source);
	infinity = getInfinity(g);
	for(i = 0; i < g->n; i++){
		/* Souce Vertex */
		if(r.distance[i] == infinity){
			printf("No path to %s from %s\n", g->cityNames[i], source);
			continue;
		}
		printf("The shortest path from %s-%s is ", source, g->cityNames[i]);
		if(i == v){
			printf("%s-%s with cost of 0\n", source, source);
			continue;
		}
		par = r.parent[i];
		while(par != -1){
			push(par, &s);
			par = r.parent[par];	
		}
		while(!isempty(&s)){
			p = pop(&s);
			printf("%s-", g->cityNames[p]);
		}
		printf("%s ", g->cityNames[i]);
		printf("with cost of : %d\n", r.distance[i]);
	}
}
void printGraph(graph *g){

}
int getVertex(graph *g, char *source){
	int i;
	for(i = 0; i < g->n; i++) {
		if(!strcasecmp(source, g->cityNames[i])) {
			return i;
		}
	}
	return -1;	//If no vertex present
}
